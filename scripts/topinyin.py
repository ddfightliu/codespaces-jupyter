import pypinyin

def topinyin(txt):
    pinyin = ''
    for i in pypinyin.pinyin(txt, style=pypinyin.NORMAL):
        pinyin += ''.join(i)
    return pinyin