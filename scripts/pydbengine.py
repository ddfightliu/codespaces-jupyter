import os
from sqlalchemy import create_engine

# os.environ['PATH']=os.environ['PATH']+';D:\\instantclient_11_2'
# os.environ['PATH'] = os.environ['PATH'] + \
#     ';/usr/local/lib/oracle/instantclient_11_2'
os.environ["NLS_LANG"] = "AMERICAN_AMERICA.UTF8"
# print(os.environ['PATH'])
# os.environ['ORACLE_HOME']='D:\\instantclient_11_2'


def oracle_engine(info="oracle+cx_oracle://XINXI:XINXI@10.76.19.33:2520/oraunix"):
    print("built a database engine : " + info)
    return create_engine(info)


def nbgs_engine(
    info="oracle+cx_oracle://nbdzs_cy3c:NBDZS_CY3C@10.76.19.96:1521/ORCL9I",
):
    print("built a database engine : " + info)
    return create_engine(
        info,
        encoding="utf-8",
    )


def mysql_engine(
    info="mysql+pymysql://root:123qweasdzxc@10.76.180.223:3306/pytrol?charset=utf8mb4",
):
    print("built a database engine : " + info)
    return create_engine(
        info,
        encoding="utf-8",
    )


def zlt_engine():
    print("built a mdb database engine logging @ XINXI ")
    return create_engine("access+pyodbc://@zlt2013")
